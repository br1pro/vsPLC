import time
import datetime
import random
import argparse
import sys
from pymodbus.constants import Defaults
from pymodbus.client import ModbusTcpClient


#Check if arguments are passed
args_count = len(sys.argv)
if args_count < 2:
    print("\nPlease specify the slave IP address, and Holding Register address, use -h option for help\n")
    sys.exit(1)

# Instantiate the parser and give it a description that will show before help
parser = argparse.ArgumentParser(description='Master modbus TCP script')

# Add arguments to the parser
parser.add_argument('-ip', dest='ip', type=str, help='slave IP address')
parser.add_argument('-hr', dest='hr', type=str, help='Hold Register to read and write')

args = parser.parse_args()
ip = args.ip
hr = int(args.hr)
print(" ----Scrip Init---- ") 
ct = datetime.datetime.now()
print(ct)
time.sleep(0.5)
#Defaults.Timeout = 1
#Defaults.Retries = 1
client = ModbusTcpClient(ip, timeout=1, retries=1)
client.connect()
time.sleep(0.5)
print(" ---- Connected to Modbus Slave ----") 
ct = datetime.datetime.now()
print(ct)

result=client.read_holding_registers(hr,1,1) #add,count,unit
data=result.registers[int(0)] 
#print(data)
ct = datetime.datetime.now()
print(ct, end="")
print("  Reading holding register", end = " ")
print(hr, end="")
print(" value = ", end= "")
print(data)
value=0
value=random.randint(1,10)
while(True):
	ct = datetime.datetime.now()
	print(ct, end="")
	print("  Writing holding register", end = " ")
	print(hr, end="")
	print(" value = ", end= "")
	print(value, end="")
	print("   --------->", end = " ")
	client.write_register(hr,value,1)
	result=client.read_holding_registers(hr,1,1) #add,count,unit
	data=result.registers[int(0)] 
	#print(data)
	ct = datetime.datetime.now()
	print(ct, end="")
	print("  Reading holding register", end = " ")
	print(hr, end="")
	print(" value = ", end= "")
	print(data, end="")
	if (value!=data):
		print("  ---------> Last write of", value, "in HR",hr," was blocked!")
	else:
		print()
	
	time.sleep(1)
	value=random.randint(1,20)
	
client.write_register(hr,0,1)
client.close()
