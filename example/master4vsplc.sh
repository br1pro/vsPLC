#!/bin/bash

#ARG1 IP
#ARG2->4 HR


ip=$1
hr0=$2
hr1=$3
hr2=$4
hr3=$5

echo "Connectiong to vsPLC $ip set registers $hr0 $hr1 $hr2 $hr3"
parallel -j2 --ungroup -j 4 -X python3 master.py ::: -ip $ip -hr $hr0 -ip $ip -hr $hr1 -ip $ip -hr $hr2 -ip $ip -hr $hr3